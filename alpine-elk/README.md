This builds Elasticsearch, Logstash, and Kibana on alpine.

## How to use
1. clone this repository
2. build using `docker build -t jjude/elk .`. Feel free to tag your images instead of jjude/elk.
3. once built, run it using, `docker run -it -p 9200:9200 -p 8080:8080 -p 5601:5601 jjude/elk /bin/bash`. Now ES is availble on 9200 & kibana on 5601. You can access kibana via http://localhost:5601