This image is a base image for all of my tiny docker containers. Currently, it's just the alpine OS image.

## How to use
1. clone this repository
2. build using `docker build -t jjude/base .`. Feel free to tag your images instead of jjude/base.
3. once built, run it using, `docker run -it jjude/base /bin/ash`.