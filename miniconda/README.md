# miniconda in a docker

copied from [CognitiveScale Repo](https://github.com/CognitiveScale/alpine-miniconda)

### How to use?

1. Install [Docker](https://docs.docker.com/engine/installation/). If you are on Mac, you can try docker beta or dlite
2. Download the Dockerfile from this repo
3. Run `docker build -t miniconda .`
4. This should download required components (alpine, python, conda) and create a docker image. Bandwidth of your ISP will determine how quickly this is completed.
5. Run `docker run -it -p 8888:8888 miniconda /bin/bash`. You have the conda environment.
6. Run `jupyter notebook --no-browser` to run jupyter