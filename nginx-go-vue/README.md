# Dockerfile for NGNIX + Vue + Golang on Alpine 3.9

## Why?

This helps to run vue and golang behind nginx, spliting vue and golang apps. This helps in deploying them separately.

For nginx Dockerfile comments, look at nginx dockerfile folder

## How to use

1. clone this repository
2. build using `docker-compose build`.
3. once built, run it using, `docker-compose up`. This starts the containers

Now you can ping `http http://localhost` and it will throw a welcome page with the message fetched from API.

Already build Go and Vue, prior to building docker containers

## Go

Build with `GOOS=linux GOARCH=amd64 go build`

## Vue

Build with `npm run build`
