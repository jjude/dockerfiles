module.exports = {
  assetsDir: "static",
  devServer: {
    proxy: "http://localhost:8080",
    port: 3000
  },
  productionSourceMap: false
};
