package main

import "github.com/astaxie/beego"

type MainController struct {
	beego.Controller
}

func (oc *MainController) Get() {
	rtnVal := make(map[string]string)
	rtnVal["code"] = "2001"
	rtnVal["message"] = "Hello from Test"
	oc.Data["json"] = rtnVal
	oc.ServeJSON()
}

func main() {
	beego.Router("/api", &MainController{})
	beego.Info("Running on ", beego.BConfig.RunMode, " and path: ", beego.AppPath)
	beego.Run()
}
