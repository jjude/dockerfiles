This image is a tiny golang container for development. It is derived from alpine. It contains golang 1.10.2

## How to use

1.  clone this repository
2.  build using `docker build -t jjude/go .`. Feel free to tag your images instead of `jjude/go`.
3.  once built, run it using, `docker run -v "$(pwd)":/go/src/app -it jjude/go /bin/sh`. This command will get you into the container. The container exposes `/go/src/app` inside the container to the current host directory. Note that once the image is built (step #2), you can run the container from any directory
4.  you can start programming in go
5.  exit using `exit` command

Refer [Docker based development environment for Golang applications](https://jjude.com/go-docker)
for more information
