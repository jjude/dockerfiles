This image is a tiny golang container for web development. It is derived from alpine. It contains golang 1.10.2, and Beego 1.9.0

## How to use

1.  clone this repository
2.  build using `docker build -t jjude/beego .`. Feel free to tag your images instead of `jjude/beego`.
3.  once built, run it using, `docker run -v "$(pwd)":/go/src/app -p 8080:8080 -it jjude/beego /bin/sh`. This command will get you into the container. The container exposes `/go/src/app` inside the container to the current host directory. Note that once the image is built (step #2), you can run the container from any directory
4.  you can start programming in go
5.  exit using `exit` command

Refer [Docker based development environment for developing web applications in Golang](https://jjude.com/docker-for-go-web) for more information
