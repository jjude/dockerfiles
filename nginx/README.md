# Dockerfile for NGNIX on Alpine 3.9

Copied from https://github.com/nginxinc/docker-nginx

## How to use
1. clone this repository
2. build using `docker build -t jjude/nginx .`. Feel free to tag your images instead of jjude/base.
3. once built, run it using, `docker run --name nginx -d -p 80:80 jjude/nginx`. This starts the image in detached mode.

Now you can ping `http http://localhost` and it will throw a welcome page.

If you want to stop `docker stop nginx` (why name is useful)
If you want to delete the container `docker rm nginx`


## Changes from official repo

### Removed modules

```
--with-mail \
--with-mail_ssl_module \
--with-compat \
--with-file-aio \
--with-http_random_index_module \
```

### Added modules

jit improves regex handling

```
--with-pcre-jit
```
