This image is a tiny nodejs container.

## How to use
1. clone this repository
2. build using `docker build -t jjude/hapi .`. Feel free to tag your images instead of jjude/hapi.
3. once built, run it using, `docker run -p 8080:8080 -d jjude/hapi`. This will run the default app and expose the app in port 8080.
4. Open another terminal and issue `curl http://localhost:8080`. It should display hello world
5. To stop the program, in another terminal issue `docker ps`. It should display the docker container id. Note the container id. Then issue `docker stop container_id`. It will stop the program.
Ref: https://lucamezzalira.com/2016/04/03/how-to-dockerize-your-node-application/