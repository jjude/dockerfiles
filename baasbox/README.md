This is a Dockerfile I use to bring-up out-of-the-box BAASBOX for local development.

### How to use?

1. Install [Docker](https://docs.docker.com/engine/installation/). If you are on Mac, you will install docker-machine.
2. Download the Dockerfile
3. Run `docker build -t baasbox .`
4. This should download required components (alpine, java, baasbox) and create a docker image. Bandwidth of your ISP will determine how quickly this is completed.
5. Run `docker run -it -p 9000:9000 baasbox`
6. Open another terminal window and issue `curl $(docker-machine ip default):9000`. `default` is the docker-machine name you used. If you created a docker-machine by other name, like 'development', then use that name.
7. You can also browse this site on your browser.