This image is a tiny redis container.

## How to use
1. clone this repository
2. build using `docker build -t jjude/redis .`. Feel free to tag your images instead of jjude/redis.
3. once built, run it using, `docker run -it jjude/base /bin/sh`.