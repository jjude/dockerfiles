# ZAP Baseline scan via docker 

Copied from [ZAProxy](https://github.com/zaproxy/zaproxy/tree/develop/build/docker)

Refer: https://github.com/zaproxy/zaproxy/wiki/ZAP-Baseline-Scan

## How to run

1. Run ```build``` to build the image