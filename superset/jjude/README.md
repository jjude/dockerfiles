# Readme

This is dockerfile for Apache Superset, which is an open-source BI tool from Airbnb.

Dockerfile is originally from https://github.com/Guru107/superset-docker/blob/master/Dockerfile

You can build it using `docker build --tag "superset" --force-rm=true --no-cache=true .`

You can run it using `docker run -p 8088:8088 --detach --name superset-container superset`

---

login into docker: `docker exec -it superset-container sh`

and create admin user like this:

`fabmanager create-admin --app superset`

Username [admin]: admin
User first name [admin]: admin
User last name [user]: user
Email [admin@fab.org]:
Password:
Repeat for confirmation:
