This image is a tiny nodejs container.

## Versions

* alpine 3.7
* node
* npm


## How to use
1. clone this repository
2. build using `docker build -t jjude/node .`. Feel free to tag your images instead of jjude/node.
3. once built, run it using, `docker run -v "$(pwd)":/usr/src/app -it jjude/node /bin/sh`. This will expose 